package ictgradschool.industry.lab09.ex03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

public class ExerciseThree_Lists {

    /**
     * Tests the union, intersection, and difference methods using some dummy data.
     *
     * You shouldn't need to edit this class, other than to uncomment two lines once you've implemented those methods.
     */
    private void start() {

        // Tip: You can make hardcoded lists in one lin like this! HOWEVER! If you try to add() extra items to these
        // lists, it will fail.
        // See: https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#asList-T...-
        List<String> testList1 = Arrays.asList("A", "B", "C");
        List<String> testList2 = Arrays.asList("A", "D", "E");

        // Should print [A, B, C, A, D, E]
        // Note #1: Arrays.toString() returns a nice String representation of any array for us to print.
        // Note #2: List.toArray() will return an Object[] array containing all the elements in our list.
        System.out.print("Union: ");
        System.out.println(Arrays.toString(union(testList1, testList2).toArray()));

        // Should print [A]
        // TODO Uncomment this line once you've completed the intersection method
        System.out.print("Intersection: ");
         System.out.println(Arrays.toString(intersection(testList1, testList2).toArray()));

        // Should print [B, C]
        // TODO Uncomment this line once you've completed the difference method
        System.out.print("Difference: ");
         System.out.println(Arrays.toString(difference(testList1, testList2).toArray()));

    }


    /**
     * Returns a list that contains all elements in the first list plus all elements in the second list.
     *
     * @param list1 the first list
     * @param list2 the second list
     * @param <T> the type of objects which can fit in the list
     * @return the result list
     */
    private <T> List<T> union(List<T> list1, List<T> list2) {

        List<T> result = new ArrayList<>();

        // TODO Add all items from list1 and list2 to the result
        for (int i = 0; i < list1.size(); i++) {
            result.add(list1.get(i));
        }

        // addAll
        result.addAll(list2);

        return result;
    }

    // TODO Write a generic intersection method
    // Check both arrays if they have the same elements then return an array with only the similarities
    private <T> List<T> intersection(List<T> list1, List<T> list2) {

        List<T> result = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            if (list1.contains(list2.get(i))) {
                result.add(list2.get(i));
            }
        }
        return result;
    }

    // TODO Write a generic difference method
    private <T> List<T> difference(List<T> list1, List<T> list2) {

        List<T> result = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            if (!list1.contains(list2.get(i))) {
                result.add(list1.get(i));
            }
        }
        return result;
    }

    /**
     * Program entry point. Do not edit.
     */

    public static void main(String[] args) {
        new ExerciseThree_Lists().start();
    }

}
